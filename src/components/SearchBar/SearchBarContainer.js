import React from "react";
import logo from "../../assests/instagram-logo.png";
import title from "../../assests/instagram-text.svg.png";
import compassIcon from "../../assests/compass-icon.png";
import heartIcon from "../../assests/instagram-heart1.png";
import peopleIcon from "../../assests/instagram-people.png";
import Icon from "./Icon";
import "./SearchBar.css";
import SearchBox from "./SearchBox";

const SearchBarContainer = () => {
  return (
    <div className="container">
      <div className="logo-div">
        <Icon src={logo} className="logo" alt="logo" />
        <Icon src={title} className="title" alt="title" />
      </div>
      <SearchBox type="text" placeholder="Search" className="search-box" />
      <div className="icons-div">
        <Icon src={compassIcon} className="compass" alt="compas" />
        <Icon src={heartIcon} className="heart" alt="heart" />
        <Icon src={peopleIcon} className="people" alt="people" />
      </div>
    </div>
  );
};

export default SearchBarContainer;
