import React from "react";
import "./SearchBar.css";

const Icon = props => {
  return <img className={props.className} src={props.src} alt={props.alt} />;
};

export default Icon;
