import React from "react";
import "./SearchBar.css";

const SearchBox = props => {
  return <input type={props.type} placeholder={props.placeholder} className={props.className}/>;
};

export default SearchBox;
