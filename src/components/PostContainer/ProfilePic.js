import React from "react";
import "./PostContainer.css";

const ProfilePic = props => {
  return <img className={props.className} src={props.src} alt={props.alt} />;
};

export default ProfilePic;
