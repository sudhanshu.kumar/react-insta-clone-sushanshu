import React from 'react';
import moment from 'moment';
const Timestamp = (props) => {
    let newTime;
    if(/August/.test(props.data.timestamp)){
        newTime = props.data.timestamp.replace(/st([^st]*)$/,"$1");
    } else{
    newTime = props.data.timestamp.replace(/th|st|nd|rd/,"");
    }
    return ( 
        <p>{moment(new Date(newTime)).fromNow()}</p>
     );
}
 
export default Timestamp;