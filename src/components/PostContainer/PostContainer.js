import React from "react";
import "./PostContainer.css";
import heartIcon from "../../assests/instagram-heart1.png";
import commentIcon from "../../assests/comment-icon.png";

const PostContainer = props => {
  return (
    <div className="post-container">
      <div className="title-box">
        <img className="profile-pic" src={props.data.thumbnailUrl} alt="dp" />
        <h4>{props.data.username}</h4>
      </div>
      <img className="post-img" src={props.data.imageUrl} alt="post-pic" />
      <div className="like-comment">
        <img src={heartIcon} alt="like" />
        <img src={commentIcon} alt="comment" />
      </div>
      <div className="likes">
        <p>{props.data.likes} likes</p>
      </div>
    </div>
  );
};

export default PostContainer;
