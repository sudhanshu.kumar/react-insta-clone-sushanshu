import React, { Component } from "react";
import "./App.css";
import SearchBarContainer from "./components/SearchBar/SearchBarContainer";
import dummyData from "./dummy-data";
import PostContainer from "./components/PostContainer/PostContainer";
import Comments from "./components/CommentSection/Comments";
import TimeStamp from "./components/CommentSection/TimeStamp";
import AddComment from "./components/CommentSection/AddComment";

class App extends Component {
  render() {
    console.log(dummyData);
    return (
      <div className="App">
        <SearchBarContainer />
        {dummyData.map(post => (
          <React.Fragment>
            <PostContainer data={post} />
            <Comments data={post} />
            <TimeStamp data={post} />
            <AddComment />
          </React.Fragment>
        ))}
      </div>
    );
  }
}

export default App;
