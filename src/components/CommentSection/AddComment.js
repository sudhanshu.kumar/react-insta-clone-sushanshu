import React from 'react';
import './Comments.css';
const AddComment = () => {
    return ( 
        <input className="add-comment" type="text" placeholder="Add comment..." />
     );
}
 
export default AddComment;