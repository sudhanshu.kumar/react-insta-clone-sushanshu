import React from "react";
import "./PostContainer.css";
const Comments = props => {
  return (
    <div className="comments">
      {props.data.comments.map(comment => {
        return (
          <p>
            <span className="username">{comment.username}</span> {comment.text}
          </p>
        );
      })}
    </div>
  );
};

export default Comments;
